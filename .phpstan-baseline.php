<?php declare(strict_types = 1);

$ignoreErrors = [];
$ignoreErrors[] = [
	'message' => '#^Call to function is_array\\(\\) with string will always evaluate to false\\.$#',
	'identifier' => 'function.impossibleType',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AjaxHandler.php',
];
$ignoreErrors[] = [
	'message' => '#^Callback expects 0 parameters, \\$accepted_args is set to 10\\.$#',
	'identifier' => 'arguments.count',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AjaxHandler.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\AjaxHandler\\:\\:__construct\\(\\) has parameter \\$post_type with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AjaxHandler.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\AjaxHandler\\:\\:save_editor_data\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AjaxHandler.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\AjaxHandler\\:\\:validate_data_from_editor\\(\\) has parameter \\$editor_data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AjaxHandler.php',
];
$ignoreErrors[] = [
	'message' => '#^Ternary operator condition is always true\\.$#',
	'identifier' => 'ternary.alwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AjaxHandler.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function is_array\\(\\) with string will always evaluate to false\\.$#',
	'identifier' => 'function.impossibleType',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AreaProperties.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\AreaProperties\\:\\:__construct\\(\\) has parameter \\$post_meta with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AreaProperties.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\AreaProperties\\:\\:set_background_color\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AreaProperties.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\AreaProperties\\:\\:set_format\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AreaProperties.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\AreaProperties\\:\\:set_height\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AreaProperties.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\AreaProperties\\:\\:set_orientation\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AreaProperties.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\AreaProperties\\:\\:set_orientation_dimensions\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AreaProperties.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\AreaProperties\\:\\:set_width\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AreaProperties.php',
];
$ignoreErrors[] = [
	'message' => '#^Offset \'r\' on \\*NEVER\\* in isset\\(\\) always exists and is not nullable\\.$#',
	'identifier' => 'isset.offset',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AreaProperties.php',
];
$ignoreErrors[] = [
	'message' => '#^Result of && is always false\\.$#',
	'identifier' => 'booleanAnd.alwaysFalse',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/AreaProperties.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\Assets\\:\\:__construct\\(\\) has parameter \\$post_type with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/Assets.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\Assets\\:\\:admin_enqueue_scripts\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/Assets.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function is_string\\(\\) with string will always evaluate to true\\.$#',
	'identifier' => 'function.alreadyNarrowedType',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/CustomizeEditPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Filter callback return statement is missing\\.$#',
	'identifier' => 'return.missing',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/CustomizeEditPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\CustomizeEditPage\\:\\:screen_layout_columns\\(\\) has parameter \\$empty_columns with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/CustomizeEditPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\CustomizeEditPage\\:\\:screen_layout_columns\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/CustomizeEditPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\CustomizeEditPage\\:\\:template_editor_callback\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/CustomizeEditPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\EditorImplementation\\:\\:__construct\\(\\) has parameter \\$post_type with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/EditorImplementation.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\EditorImplementation\\:\\:get_post_meta\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/EditorImplementation.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\EditorImplementation\\:\\:post_type_args_definition\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/EditorImplementation.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\EditorImplementation\\:\\:set_post_type\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/EditorImplementation.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$post_id \\(int\\) of method WPDesk\\\\Library\\\\WPCanvaEditor\\\\EditorImplementation\\:\\:get_area_properties\\(\\) should be contravariant with parameter \\$post_id \\(mixed\\) of method WPDesk\\\\Library\\\\CouponInterfaces\\\\EditorIntegration\\:\\:get_area_properties\\(\\)$#',
	'identifier' => 'method.childParameterType',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/EditorImplementation.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$post_id \\(int\\) of method WPDesk\\\\Library\\\\WPCanvaEditor\\\\EditorImplementation\\:\\:get_post_meta\\(\\) should be contravariant with parameter \\$post_id \\(mixed\\) of method WPDesk\\\\Library\\\\CouponInterfaces\\\\EditorIntegration\\:\\:get_post_meta\\(\\)$#',
	'identifier' => 'method.childParameterType',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/EditorImplementation.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\WPCanvaEditor\\\\RegisterPostType\\:\\:get_post_type_args\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/RegisterPostType.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$editor_data has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/Views/html-editor-meta-box.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$text of function esc_attr expects string, int given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/Views/html-editor-meta-box.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WP_Post\\:\\:\\$ID \\(int\\) in isset\\(\\) is not nullable\\.$#',
	'identifier' => 'isset.property',
	'count' => 1,
	'path' => __DIR__ . '/src/Editor/Views/html-editor-meta-box.php',
];

return ['parameters' => ['ignoreErrors' => $ignoreErrors]];
