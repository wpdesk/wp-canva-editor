<?php
/**
 * Editor. Assets.
 *
 * @package WPDesk\Library\WPCanvaEditor
 */

namespace WPDesk\Library\WPCanvaEditor;

use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Enqueue editor scripts.
 *
 * @package WPDesk\Library\WPCanvaEditor
 */
class Assets implements Hookable {

	/**
	 * @var string
	 */
	private $post_type;

	/**
	 * @var string
	 */
	protected $scripts_version = '1.3';

	/**
	 * @param $post_type
	 */
	public function __construct( $post_type ) {
		$this->post_type = $post_type;
	}

	/**
	 * Fires hooks
	 */
	public function hooks() {
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ] );
	}

	/**
	 * @return string
	 */
	protected function get_assets_url() {
		return trailingslashit( plugin_dir_url( __DIR__ ) ) . 'assets/';
	}

	/**
	 * Enqueue editor scripts and styles.
	 */
	public function admin_enqueue_scripts() {
		$screen = get_current_screen();
		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		$is_pl                = 'pl_PL' === get_locale();
		$pro_url              = $is_pl ? 'https://www.wpdesk.pl/sklep/flexible-coupons-woocommerce/?utm_source=wp-admin-plugins&utm_medium=link&utm_campaign=flexible-coupons-pro&utm_content=edit-template' : 'https://wpdesk.net/products/flexible-coupons-woocommerce/?utm_source=wp-admin-plugins&utm_medium=link&utm_campaign=flexible-coupons-pro&utm_content=edit-template';
		$shortcodes_pro_url   = $is_pl ? 'https://wpde.sk/fc-codes-up-pl' : 'https://wpde.sk/fc-codes-up';
		$tickets_buy_url      = $is_pl ? 'https://wpde.sk/fc-qr-codes-pl-up' : 'https://wpde.sk/fc-qr-codes-up';
		$tickets_docs_url     = $is_pl ? 'https://wpde.sk/fc-qr-codes-pl-docs' : 'https://wpde.sk/fc-qr-codes-docs';
		$sending_buy_url      = $is_pl ? 'https://wpde.sk/as-advanced-sending-template-pl' : 'https://wpde.sk/as-advanced-sending-template';
		$sending_settings_url = admin_url( 'edit.php?post_type=wpdesk-coupons&page=fc-settings&tab=emails' );

		if ( 'post' === $screen->base && $this->post_type === $screen->post_type ) {

			if ( ! is_rtl() ) {
				wp_enqueue_style( 'canva-editor-admin', $this->get_assets_url() . 'css/admin.css', [], $this->scripts_version );
			} else {
				wp_enqueue_style( 'canva-editor-admin-rtl', $this->get_assets_url() . 'css/admin-rtl.css', [], $this->scripts_version );
			}

			wp_enqueue_media();
			wp_register_script( 'wp-canva-editor', $this->get_assets_url() . 'js/wpdesk-canva-editor' . $suffix . '.js', [ 'wp-editor' ], $this->scripts_version, true );
			wp_enqueue_script( 'wp-canva-editor' );
			wp_localize_script(
				'wp-canva-editor',
				'wpdesk_canva_editor_lang',
				[
					'general'                    => esc_html__( 'General', 'wp-canva-editor' ),
					'images'                     => esc_html__( 'Images', 'wp-canva-editor' ),
					'text'                       => esc_html__( 'Text', 'wp-canva-editor' ),
					'shortcodes'                 => esc_html__( 'Shortcodes', 'wp-canva-editor' ),
					'qrcode'                     => esc_html__( 'QR Code', 'wp-canva-editor' ),
					'select_format'              => esc_html__( 'Select format', 'wp-canva-editor' ),
					'page_orientation'           => esc_html__( 'Page orientation', 'wp-canva-editor' ),
					'vertical'                   => esc_html__( 'Vertical', 'wp-canva-editor' ),
					'horizontal'                 => esc_html__( 'Horizontal', 'wp-canva-editor' ),
					'background_color'           => esc_html__( 'Background color', 'wp-canva-editor' ),
					'color'                      => esc_html__( 'Color', 'wp-canva-editor' ),
					'select'                     => esc_html__( 'Select', 'wp-canva-editor' ),
					'select_images'              => esc_html__( 'Select images', 'wp-canva-editor' ),
					'add_to_area'                => esc_attr__( 'Add to area', 'wp-canva-editor' ),
					'remove_from_project'        => esc_attr__( 'Remove from project', 'wp-canva-editor' ),
					'edit'                       => esc_attr__( 'Edit', 'wp-canva-editor' ),
					'fit_to_screen'              => esc_attr__( 'Fit to screen', 'wp-canva-editor' ),
					'crop'                       => esc_attr__( 'Crop', 'wp-canva-editor' ),
					'layer_up'                   => esc_attr__( 'Layer up', 'wp-canva-editor' ),
					'layer_down'                 => esc_attr__( 'Layer down', 'wp-canva-editor' ),
					'clone_element'              => esc_attr__( 'Clone element', 'wp-canva-editor' ),
					'delete_element'             => esc_attr__( 'Delete element', 'wp-canva-editor' ),
					'delete'                     => esc_attr__( 'Delete element', 'wp-canva-editor' ),
					'change_image'               => esc_attr__( 'Change image', 'wp-canva-editor' ),
					'align_right'                => esc_attr__( 'Align right', 'wp-canva-editor' ),
					'align_left'                 => esc_attr__( 'Align left', 'wp-canva-editor' ),
					'align_center'               => esc_attr__( 'Align center', 'wp-canva-editor' ),
					'justify'                    => esc_attr__( 'Justify', 'wp-canva-editor' ),
					'font_size'                  => esc_attr__( 'Font size', 'wp-canva-editor' ),
					'change_font_size'           => esc_attr__( 'Change font size', 'wp-canva-editor' ),
					'font_family'                => esc_attr__( 'Font family', 'wp-canva-editor' ),
					'change_font_family'         => esc_attr__( 'Change font family', 'wp-canva-editor' ),
					'double_click_to_edit'       => esc_attr__( 'Double click to edit', 'wp-canva-editor' ),
					'font_color'                 => esc_attr__( 'Font color', 'wp-canva-editor' ),
					'header1'                    => esc_html__( 'Header 1', 'wp-canva-editor' ),
					'header2'                    => esc_html__( 'Header 2', 'wp-canva-editor' ),
					'header3'                    => esc_html__( 'Header 3', 'wp-canva-editor' ),
					'content'                    => esc_html__( 'Content', 'wp-canva-editor' ),
					'upgrade_to_pro'             => sprintf(
						/* translators: %1$s: anchor opening tag, %2$s: anchor closing tag */
						esc_html__( '%1$sUpgrade to PRO →%2$s and enable more shortcodes', 'wp-canva-editor' ),
						'<a href="' . esc_url( $pro_url ) . '" target="_blank" class="pro-link">',
						'</a>'
					),
					'coupons_pro_plugin_enabled' => $this->is_active( 'flexible-coupons-pro/flexible-coupons-pro.php' ) ? 'yes' : 'no',
					'pro_plugin_name'            => esc_html__( 'Flexible Coupons PRO', 'wp-canva-editor' ),
					'pro_addon_info'             => sprintf(
						/* translators: %1$s: anchor opening tag, %2$s: anchor closing tag */
						esc_html__( 'This is an add-on for the PRO version. %1$sUpgrade to PRO →%2$s', 'wp-canva-editor' ),
						'<a href="' . esc_url( $pro_url ) . '" target="_blank" class="pro-link">',
						'</a>'
					),
					'shortcodes_plugin_link'     => sprintf(
						/* translators: %1$s: anchor opening tag, %2$s: anchor closing tag */
						esc_html__( 'Add custom shortcodes with Flexible PDF Coupons PRO addon: %1$sCustom Shortcodes →%2$s', 'wp-canva-editor' ),
						'<a href="' . esc_url( $shortcodes_pro_url ) . '" target="_blank" class="shortcode-link">',
						'</a>'
					),
					'shortcodes_plugin_enabled'  => $this->is_active( 'flexible-coupons-shortcodes/flexible-coupons-shortcodes.php' ) ? 'yes' : 'no',
					'tickets_plugin_label'       => esc_html__( 'Place a generated QR code on the template. Enable users to manage QR codes with the Event Ticket QR Scanner.', 'wp-canva-editor' ),
					'tickets_docs_label'         => esc_html__( 'Read more in plugin docs', 'wp-canva-editor' ),
					'tickets_docs_url'           => $tickets_docs_url,
					'tickets_plugin_buy_label'   => esc_html__( 'Add QR codes with Flexible PDF Coupons PRO addon', 'wp-canva-editor' ),
					'tickets_buy_url'            => $tickets_buy_url,
					'tickets_plugin_name'        => esc_html__( 'Event Ticket QR Scanner', 'wp-canva-editor' ),
					'tickets_plugin_enabled'     => $this->is_active( 'flexible-coupons-tickets/flexible-coupons-tickets.php' ) ? 'yes' : 'no',
					'tickets_upgrade_pro_label'  => esc_html__( 'To use QR codes you need', 'wp-canva-editor' ),
					'sending_plugin_enabled'     => $this->is_active( 'flexible-coupons-sending/flexible-coupons-sending.php' ) ? 'yes' : 'no',
					'sending_plugin_buy_label'   => esc_html__( 'Send coupons by email and schedule gift card delivery with Flexible PDF Coupons PRO addon', 'wp-canva-editor' ),
					'sending_buy_url'            => $sending_buy_url,
					'sending_plugin_name'        => esc_html__( 'Advanced Sending', 'wp-canva-editor' ),
					'sending_plugin_label'       => esc_html__( 'Send coupons by email and schedule gift card delivery', 'wp-canva-editor' ),
					'sending_settings_url'       => $sending_settings_url,
					'sending'                    => esc_html__( 'Sending', 'wp-canva-editor' ),
				]
			);

			wp_register_script( 'wp-canva-admin', $this->get_assets_url() . 'js/admin.js', [ 'wp-canva-editor' ], $this->scripts_version, true );
			wp_enqueue_script( 'wp-canva-admin' );
			wp_localize_script(
				'wp-canva-admin',
				'wp_canva_admin',
				[
					'post_type' => $this->post_type,
					'nonce'     => wp_create_nonce( 'editor_save_post_' . $this->post_type ),
					'lang'      => get_locale(),
				]
			);
		}
	}

	/**
	 * @param string $plugin
	 *
	 * @return bool
	 */
	public function is_active( string $plugin ): bool {
		if ( function_exists( 'is_plugin_active_for_network' ) && is_plugin_active_for_network( $plugin ) ) {
			return true;
		}

		return in_array( $plugin, (array) get_option( 'active_plugins', [] ), true );
	}
}
