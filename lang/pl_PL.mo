��    >        S   �      H  5   I  ;     0   �  X   �     E     Q     b  
   o     z     �     �     �     �     �     �     �     �     �     �          "     '     .     C     [     i  
   ~     �  	   �     �     �     �     �  
   �     �     �  
   �     �     �     	  l        �     �     �     �     �     �     �     �  5   �  Y   )	  
   �	  1   �	     �	  A   �	     
      
     )
     E
     W
     u
    �
  =   �  ;   �  :   #  c   ^     �     �  
   �     �       
        &     <     T     a     p     v     ~     �     �  #   �     �     �     �     �       #         D     S     b     r     {     �     �     �     �     �     �     �     �     �  }        �  )   �     �     �     �     �     �     �  :   
  g   E  	   �  +   �     �  8   �  0   "     S     [     b     i     p               (   /   2   )   '   #   1   :   ;   6   >          !                        
       	      -       0           7         &   9             .            "      <      5      ,                $                                  %   +           4           3   *                =       8        %1$sUpgrade to PRO →%2$s and enable more shortcodes %1$sUpgrade to PRO →%2$s to fully unlock all the features Add QR codes with Flexible PDF Coupons PRO addon Add custom shortcodes with Flexible PDF Coupons PRO addon: %1$sCustom Shortcodes →%2$s Add to area Advanced Sending Align center Align left Align right Background color Change font family Change font size Change image Clone element Color Content Crop Data cannot be saved Delete element Double click to edit Edit Editor Enter template title Event Ticket QR Scanner Fit to screen Flexible Coupons PRO Font color Font family Font size General Header 1 Header 2 Header 3 Horizontal Images Justify Layer down Layer up Manage editor templates. Page orientation Place a generated QR code on the template. Enable users to manage QR codes with the Event Ticket QR Scanner. QR Code Read more in plugin docs Remove from project Save template Saved Select Select format Select images Send coupons by email and schedule gift card delivery Send coupons by email and schedule gift card delivery with Flexible PDF Coupons PRO addon Shortcodes Template does not contain [coupon_code] shortcode Text This is an add-on for the PRO version. %1$sUpgrade to PRO →%2$s To use QR codes you need Vertical add new on admin barEditor admin menuEditor post type general nameEditor post type singular nameEditor Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-19 08:02+0200
Last-Translator: 
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0.1
X-Domain: wp-canva-editor
 %1$sUlepsz do PRO →%2$s aby korzystać z innych szortkodów %1$sUlepsz do PRO →%2$s aby odblokować wszystkie funkcje Dodaj kody QR z pomocą dodatku Kupony PDF WooCommerce PRO Dodaj własne shortcody za pomocą dodatku Elastyczne Kupony PDF PRO: %1$sWłasne Szortkody →%2$s Dodaj do obszaru Zaawansowana Wysyłka Wyśrodkuj Wyrównaj do lewej Wyrównaj do prawej Kolor tła Zmień krój czcionki Zmień rozmiar czcionki Zmień obraz Klonuj element Kolor Treść Przytnij Nie można zapisać danych Usuń element Podwójne kliknięcie edytuje tekst Edytuj Edytor Wpisz tytuł szablonu Skaner Biletów QR Wyrównaj do obszaru Kupony PDF WooCommerce w wersji PRO Kolor czcionki Krój czcionki Rozmiar czcinki Główne Nagłówek 1 Nagłówek 2 Nagłówek 3 Pozioma Obrazki Wyjustuj Warstwa niżej Warstwa wyżej Zarządzaj szablonami edytora. Orientacja wydruku Umieść wygenerowany kod QR na szablonie. Pozwól użytkownikom zarządzać kodami QR za pomocą dodatku Skaner Biletów QR. Kod QR Przeczytaj więcej w dokumentacji wtyczki Usuń z projektu Zapisz szablon Zapisano Wybierz Wybierz format Wybierz obrazki Wysyłaj kupony mailem i planuj dostawy kart podarunkowych Wysyłaj kupony mailem i planuj dostawy kart podarunkowych dzięki dodatkowi Kupony PDF WooCommerce PRO Szortkody Szablon nie zawiera szortkodu [coupon_code] Tekst To jest dodatek do wersji PRO. %1$sUlepsz do PRO →%2$s Aby korzystać z kodów QR, potrzebujesz wtyczki Pionowa Edytor Edytor Edytor Edytor 