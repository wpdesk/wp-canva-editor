## [1.4.8] - 2024-02-19
### Fixed 
* Phpcs issues

## [1.4.7] - 2024-12-23
### Fixed
* Can not add new coupon templates (fatal error)

## [1.4.6] - 2024-12-13
### Fixed
* Potential XSS vulnerability in template editor

## [1.4.5] - 2024-12-09
### Fixed
* XSS vulnerabilitie

## [1.4.4] - 2024-11-19
### Fixed
* Translations loading to early in wp 6.7

## [1.4.3] - 2024-04-19
### Fixed
* Polish translation placeholder error

## [1.4.2] - 2024-04-18
### Fixed
* Marketing links, styles and text

## [1.4.1] - 2023-09-19
### Fixed
- links
- translations

## [1.4.0] - 2023-08-10
### Added
- qr codes tab
- advanced sending tab

## [1.3.2] - 2023-07-10
### Fix
- screen_layout_columns filter return type

## [1.3] - 2022-10-07
### Added
- docs url
- pro url

## [1.3] - 2022-08-18
### Added
- font sizes

## [1.2.3] - 2020-10-11
### Fix
- library update

## [1.2.2] - 2020-12-16
### Fixed
- fixed font render

## [1.2.1] - 2020-10-18
### Fixed
- filter images in wp.media editor
- rtl support

## [1.2.0] - 2020-06-18
### Fixed
- fixed problems with coupon implementation
- refactor edges detector for all objects inside area when selected object is resized or moved.
- added shared interfaces

## [1.1.1] - 2020-06-10
### Fixed
- remove unused code, refactor menu text objects

## [1.1.0] - 2020-06-06
### Fixed
- implements custom texts

## [1.0.0] - 2020-06-06
### Added
- init
